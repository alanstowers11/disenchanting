2.2.2 (Release)

- Fixed bug where hotswapping enchanted item would allow you to take an enchantment from the swapped-out item for free.

md5 1.19.3: 2433f1c0d308232aa80b8142ab02b8a4 \
md5 1.19: bf09d5714a3a3d0ebefd4b4c8a352c66 \
md5 1.18: 4c2f1215779eabe1c14da3e67e75884f \


2.2.1 (Release)

1.19.3 release
Disenchanting checks if item is damageable before damaging/destroying if config `damageItem` was enabled. Before it would destroy enchanted books.

md5 1.19.3: 10179b46d464d54dfe74feda10b143b7 
md5 1.19: 531f9581d0f11656a9dad81152fafa4f 
md5 1.18: e7b257d0570055880d399a5c7411ef7d

2.2.1 (Release)

1.19.3 release
Disenchanting checks if item is damageable before damaging/destroying if config damageItem was enabled. Before it would destroy enchanted books.

md5 1.19.3: 10179b46d464d54dfe74feda10b143b7 \
md5 1.19: 531f9581d0f11656a9dad81152fafa4f \
md5 1.18: e7b257d0570055880d399a5c7411ef7d

2.2.0 (Release) \
Added:
 - New configs damageItem and damagePercent control whether or not the item should be damaged in the disenchanting process.

Fixed:
 - [Issue #23](https://gitlab.com/chirptheboy/disenchanting/-/issues/23): Item's anvil cost was being reset after each round of disenchanting, not waiting until the final enchantment was removed. A side effect was an item being fully repaired using this option.

md5 1.19:  
md5 1.18: 

2.1.0 (Release)
- Added blacklist for enchantments, pre-populated with the two vanilla curses
- UI shows red X when trying to disenchant a blacklisted enchantment (like anvil)
- Fixed UI being cut off on the right

md5: 481cd27b47de38d3509902fe53b914a2 (1.19)
md5: 69b1ca43efb6e61cf4cbec3bee8d6640 (1.18)

- 2.0.0 (Release)
Official release for 1.18
- New 3D block model
- Added disenchantment sound effect
- Added "disenchanter_immune" data file
- Fixed bug where enchanted books could be disenchanted indefinitely
md5: e996848fc2b016955d725c578310ace9

2.0.0-alpha
First 1.18 release.
Added:
 - New config resetAnvilCost: Resets the anvil cost to zero when the final enchantment is removed from an item.
Fixed:
 - Enchanted books can no longer be extracted with hoppers/conduits/etc, fixing the duplication bug.
Known issues:
 - Removing enchanted books by shift-clicking doesn't automatically create the next enchanted book, if one can be made.
 - When using the disenchanter, shift-clicking doesn't move items between the hotbar and inventory.
 - No disenchanting sound occurs.
md5: 611e5c3c22ade63d4c367e0efba55921

1.7.3 (Release)
 - Update to 1.17

1.7.2 (Patch)
- Added Chinese translation (!3)
MD5 signature: 5c00f0aa869ead03e2943ccaba2c1c1c

1.7.1 (Patch)
 - Added Portuguese translation (!2)
 MD5 signature: 36be9eef462be8d03d430fa229015773

1.7.0 (Release)
 - #14: Fixed bad GUI border and size
 - #17: Disallow Quark's Ancient Tome from being disenchanted
MD5 signature: 57e059bf0033d753d32289ccf00cac3b

1.5.0 (Bugfix)
 - Fixed experience cost when in creative mode
MD5 signature: 69404435fb7820edeaae5962c0d2d1e4 (1.14)
MD5 signature: 075039d67bd9133440a786c76b6a6fb9 (1.15)

1.4.0 (Release)
 - Added random enchantment functionality and corresponding config
MD5 signature: 8db018f4dd2f66e4c986045c75faee87 (1.14)
MD5 signature: 20303f93802c88922bd6a118f78a7b2b (1.15)

1.3.0 (Patch)
 - #11: Fixed infinite enchantments bug
 - Removed parentheses from filename
MD5 signature: 5b7cf471afb0ad7509b8bb0eba1f8aff (1.14)
MD5 signature: 7e98a100cd28557306d5c281b1b18ea4 (1.15)

1.2.0 (Release)
 - Initial release for 1.15.2
 - Added configs
 - Added configurable enchantment cost
MD5 signature: f36a70ccb039fdcdb11dbf5aa24022ce (1.14)
MD5 signature: 2ef3229cb411d4aba2ddc8c898923607 (1.15)


1.1.2 (Patch)
 - Fixed shift-click taking of enchanted books
 - Modified top/side textures
 - Changed bottom texture to obsidian default
MD5 signature:6b7905cddc9f97ee472ac745176cc1af

1.1.1 (Patch)
Bugfixes:
 - #9: Last enchantment not being removed
 - #4: Removed debugging code and some comments
MD5 signature: 91a036d38dab4d53ed73b1b100913f66

1.1.0 (Release)
Bugfixes:
 - #1: Fixed right-click issues (consuming torches)
New features:
- Disenchanter is now visually more like the vanilla enchanting table
MD5 signature: 945dbe6de3ba0238d60f260fcad8d4fc

1.0.1 (Patch)
- Fixed #2: Output book not updated when hotswapping (dragging) enchanted items
- Fixed #3: First enchantment not removed from book when disenchanting an enchanted book
MD5 signature: 1dd3cd5a3f04f794b199b0b295f041f4

1.0.0 (Release)
- Complete rewrite of the container using proper handlers
- Fixed all existing bugs
MD5 signature: ec77a39a69678a70ab0423f4d1dae225

0.1.0 (Alpha)
- Initial release using slot-checking for updates
MD5 signature: 889a60e4f4061e41bab3cf07441a19c1
