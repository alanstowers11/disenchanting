package com.chirptheboy.disenchanting.block.disenchant;

import com.chirptheboy.disenchanting.block.TileBlockEntityDisenchanting;
import com.chirptheboy.disenchanting.capabilities.ItemStackHandlerWrapper;
import com.chirptheboy.disenchanting.data.DataTags;
import com.chirptheboy.disenchanting.registry.TileRegistry;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.world.MenuProvider;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.enchantment.Enchantment;
import net.minecraft.world.item.enchantment.EnchantmentHelper;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.common.ForgeConfigSpec.BooleanValue;
import net.minecraftforge.common.ForgeConfigSpec.IntValue;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.ItemHandlerHelper;
import net.minecraftforge.items.ItemStackHandler;
import org.jetbrains.annotations.NotNull;

import java.util.Map;

public class TileDisenchant extends TileBlockEntityDisenchanting implements MenuProvider {

  static enum Fields {
    REDSTONE, TIMER, COST, SHOWCOST, COLOR;
  }

  public static final int SLOT_INPUT = 0;
  public static final int SLOT_BOOK = 1;


  ItemStackHandler inputSlots = new ItemStackHandler(2) {

    @Override
    public boolean isItemValid(int slot, ItemStack stack) {
      if (slot == SLOT_BOOK) {
        return stack.getItem() == Items.BOOK;
      }
      else if (slot == SLOT_INPUT) {
        Map<Enchantment, Integer> enchants = EnchantmentHelper.getEnchantments(stack);
        return enchants != null && enchants.size() > 0 && !stack.is(DataTags.DISENCHANTER_IMMUNE);
      }
      return stack.getItem() == Items.ENCHANTED_BOOK;
    }
  };

  ItemStackHandler outputSlots = new ItemStackHandler(1) {
    @NotNull
    @Override
    public ItemStack extractItem(int slot, int amount, boolean simulate) {
      {
        if (amount == 0)
          return ItemStack.EMPTY;

        validateSlotIndex(slot);

        ItemStack existing = this.stacks.get(slot);

        if (existing.isEmpty())
          return ItemStack.EMPTY;

        int toExtract = Math.min(amount, existing.getMaxStackSize());

        if (existing.getCount() <= toExtract)
        {
          if (!simulate)
          {
            // Removed this because I want to do it in the container (so I know which enchantment to remove)
            this.stacks.set(slot, ItemStack.EMPTY);
            onContentsChanged(slot);
            return existing;
          }
          else
          {
            return existing.copy();
          }
        }
        else
        {
          if (!simulate)
          {
            this.stacks.set(slot, ItemHandlerHelper.copyStackWithSize(existing, existing.getCount() - toExtract));
            onContentsChanged(slot);
          }
          return ItemHandlerHelper.copyStackWithSize(existing, toExtract);
        }
      }
    }
  };
  private ItemStackHandlerWrapper inventory = new ItemStackHandlerWrapper(inputSlots, outputSlots);
  private final LazyOptional<IItemHandler> inventoryCap = LazyOptional.of(() -> inventory);

  public static BooleanValue REQUIRES_EXPERIENCE;
  public static BooleanValue RANDOM_ENCHANTMENT;
  public static BooleanValue RESET_ANVIL_COST;
  public static BooleanValue DAMAGE_ITEM;
  public static IntValue     COST_SLIDER;
  public static IntValue     DAMAGE_PERCENT;

  public TileDisenchant(BlockPos pos, BlockState state) {
    super(TileRegistry.DISENCHANTER.get(), pos, state);
  }

  @Override
  public Component getDisplayName() {
    return new TextComponent(getType().getRegistryName().getPath());
  }

  @Override
  public AbstractContainerMenu createMenu(int i, Inventory playerInventory, Player playerEntity) {
    return new ContainerDisenchant(i, level, worldPosition, playerInventory, playerEntity);
  }

  @Override
  public void invalidateCaps() {
    inventoryCap.invalidate();
    super.invalidateCaps();
  }

  // Remove to fix the duplication bug, but breaks dropping inventory on block removal
  @Override
  public <T> LazyOptional<T> getCapability(Capability<T> cap, Direction side) {
    if (cap == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY) {
      return inventoryCap.cast();
    }
    return super.getCapability(cap, side);
  }

  @Override
  public void load(CompoundTag tag) {
    inventory.deserializeNBT(tag.getCompound(NBTINV));
    super.load(tag);
  }

  @Override
  public void saveAdditional(CompoundTag tag) {
    tag.put(NBTINV, inventory.serializeNBT());
    CompoundTag fluid = new CompoundTag();
    tag.put(NBTFLUID, fluid);
    super.saveAdditional(tag);
  }

  @Override
  public void setField(int field, int value) {
    switch (Fields.values()[field]) {
      case REDSTONE:
        this.needsRedstone = value % 2;
      break;
      case TIMER:
        timer = value;
      break;
      case COST:
        cost = value;
      case SHOWCOST:
        showCost = value;
      case COLOR:
        color = value;
      break;
    }
  }

  @Override
  public int getField(int field) {
    switch (Fields.values()[field]) {
      case REDSTONE:
        return needsRedstone;
      case TIMER:
        return timer;
      case COST:
        return cost;
      case SHOWCOST:
        return showCost;
      case COLOR:
        return color;
    }
    return 0;
  }
}
