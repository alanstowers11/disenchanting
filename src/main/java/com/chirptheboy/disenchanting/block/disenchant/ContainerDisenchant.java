package com.chirptheboy.disenchanting.block.disenchant;

import com.chirptheboy.disenchanting.gui.ContainerBase;
import com.chirptheboy.disenchanting.registry.BlockRegistry;
import com.chirptheboy.disenchanting.registry.ContainerScreenRegistry;
import com.google.common.collect.ImmutableMap;
import net.minecraft.core.BlockPos;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.sounds.SoundSource;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.ContainerLevelAccess;
import net.minecraft.world.inventory.Slot;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.enchantment.Enchantment;
import net.minecraft.world.item.enchantment.EnchantmentHelper;
import net.minecraft.world.level.Level;
import net.minecraftforge.items.ItemStackHandler;
import net.minecraftforge.items.SlotItemHandler;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

public class ContainerDisenchant extends ContainerBase {

  protected TileDisenchant tile;
  public static double RARITY_MULTIPLIER = 0.03F;
  public static double RARITY_OFFSET = 1.8F;

  public ContainerDisenchant(int windowId, Level world, BlockPos pos, Inventory playerInventory, Player player) {
    super(ContainerScreenRegistry.DISENCHANTER, windowId);
    tile = (TileDisenchant) world.getBlockEntity(pos);
    this.playerEntity = player;
    this.playerInventory = playerInventory;
    this.trackAllIntFields(tile, TileDisenchant.Fields.values().length);
    ItemStackHandler inputSlots = tile.inputSlots;
    ItemStackHandler outputSlots = tile.outputSlots;

    this.endInv = inputSlots.getSlots() + outputSlots.getSlots();

    /**   ENCHANTED ITEM SLOT*/
    addSlot(new SlotItemHandler(inputSlots, 0, 29, 25) {// +2

      @Override
      public void setChanged() {
        createOutputItem();
        tile.setChanged();
      }
    });
    /**   BOOK SLOT*/
    addSlot(new SlotItemHandler(inputSlots, 1, 83, 25) { // +3

      @Override
      public void setChanged() {
        createOutputItem();
        tile.setChanged();
      }
    });
    /**   OUTPUT SLOT*/
    addSlot(new DisenchantOutputSlot(outputSlots, 0, 137, 25) { // +4

      @Override
      public void onTake(Player player, ItemStack itemStack) {
        disenchantItem(player, itemStack);
        this.setChanged();
        tile.setChanged();
        createOutputItem();
      }

      @Override
      public void setChanged() {
        tile.setChanged();
      }
    });
    layoutPlayerInventorySlots(11, 71);
    this.trackAllIntFields(tile, TileDisenchant.Fields.values().length);
  }



  public void disenchantItem(Player player, ItemStack itemEnchantedBook){

    Boolean itemBroke = false;
    BlockPos p = this.tile.getBlockPos();

    ItemStack inputEnchantedItem = tile.inputSlots.getStackInSlot(TileDisenchant.SLOT_INPUT);

      // Deduct experience if required by config and not in creative mode
      if (TileDisenchant.REQUIRES_EXPERIENCE.get() && !player.isCreative())
        player.giveExperienceLevels(-tile.getCost());

      // Since the itemStack is the newly-enchanted book, it should only have one enchantment
      Enchantment enchantmentToRemove = getEnchantmentFromItem(itemEnchantedBook);

      // Create a map from the enchanted item in the input slot
      Map<Enchantment, Integer> enchantmentMap = EnchantmentHelper.getEnchantments(tile.inputSlots.getStackInSlot(0));
      int numEnchantments = enchantmentMap.size();
      enchantmentMap.remove(enchantmentToRemove);

      /** Create a copy of the input item as the output item to go in its place. Using 'copy' to get the damage,
       *  then updating the repair cost if the config is set AND it's the last enchantment being removed. */
      ItemStack outputItem = inputEnchantedItem.copy();
      if (TileDisenchant.RESET_ANVIL_COST.get() && numEnchantments == 1) outputItem.setRepairCost(0);
      EnchantmentHelper.setEnchantments(enchantmentMap, outputItem);

    // Damage or destroy the item if configured to
    if (TileDisenchant.DAMAGE_ITEM.get() && outputItem.isDamageableItem()) {

      double durabilityToReduceBy = (double)outputItem.getMaxDamage() * ((double)(TileDisenchant.DAMAGE_PERCENT.get()) / 100);
      int currentDurability = outputItem.getMaxDamage() - outputItem.getDamageValue();
      int newDamage = outputItem.getDamageValue() + (int)durabilityToReduceBy;
      int newDurability = currentDurability - (int)durabilityToReduceBy;

      // If damaged too much, its destroyed
      if (newDurability < 1) {
        outputItem = new ItemStack(Items.AIR);
        itemBroke = true;
      } else {
        outputItem.setDamageValue(newDamage);
      }
    }

    // Override the output for books
      if (outputItem.is(Items.ENCHANTED_BOOK)) {
        if (enchantmentMap.size() == 0)
          outputItem = new ItemStack(Items.BOOK);
        else {
          outputItem = new ItemStack(Items.ENCHANTED_BOOK);
          EnchantmentHelper.setEnchantments(enchantmentMap, outputItem);
        }
      }

      // Play the sound
      tile.getLevel().playLocalSound(p.getX(), p.getY(), p.getZ(), itemBroke ? SoundEvents.ITEM_BREAK : SoundEvents.ENCHANTMENT_TABLE_USE, SoundSource.BLOCKS, 1.0F, 1.25F, false);

    // Update the input slots
      tile.inputSlots.setStackInSlot(TileDisenchant.SLOT_INPUT, outputItem);
      tile.inputSlots.extractItem(TileDisenchant.SLOT_BOOK, 1, false);
  }

  public void createOutputItem() {
    if (!playerEntity.level.isClientSide()) {

      ItemStack input = this.tile.inputSlots.getStackInSlot(0);
      ItemStack books = this.tile.inputSlots.getStackInSlot(1);

      // If both input slots are valid
      if (isItemEnchanted(input) && books.is(Items.BOOK)) {
        Enchantment selectedEnchantment;
        int selectedEnchantmentLevel = 0;
        int cost = 0;
        boolean hasEnoughXP = false;

        selectedEnchantment = getEnchantmentFromItem(input);
        selectedEnchantmentLevel = getEnchantmentLevelFromItem(selectedEnchantment, input);

        // Only calculate cost if required
        if (TileDisenchant.REQUIRES_EXPERIENCE.get() && !playerEntity.isCreative()) {
          int rarity = selectedEnchantment.getRarity().getWeight();
          int maxLevel = selectedEnchantment.getMaxLevel();
          cost = (int) ((RARITY_OFFSET - (rarity * RARITY_MULTIPLIER) * (selectedEnchantmentLevel / maxLevel)) * TileDisenchant.COST_SLIDER.get());
          hasEnoughXP = playerEntity.experienceLevel >= cost;
          tile.setField(TileDisenchant.Fields.COST.ordinal(), cost);
          tile.setField(TileDisenchant.Fields.SHOWCOST.ordinal(), 1);
        } else {
          tile.setField(TileDisenchant.Fields.SHOWCOST.ordinal(), 0);
        }

        // Create the enchanted book
        // If xp isn't required OR it's required but you have enough OR you're in creative mode
        if (!TileDisenchant.REQUIRES_EXPERIENCE.get() || (TileDisenchant.REQUIRES_EXPERIENCE.get() && hasEnoughXP) || playerEntity.isCreative()) {
          ItemStack enchantedBook = new ItemStack(Items.ENCHANTED_BOOK);
          EnchantmentHelper.setEnchantments(ImmutableMap.of(selectedEnchantment, selectedEnchantmentLevel), enchantedBook);
          this.tile.outputSlots.setStackInSlot(0, enchantedBook);
        }

      } else {
        this.tile.outputSlots.setStackInSlot(0, ItemStack.EMPTY);
        this.tile.setField(TileDisenchant.Fields.SHOWCOST.ordinal(), 0);
        this.tile.setField(TileDisenchant.Fields.COST.ordinal(), 0);
      }
    }
  }

  public Boolean isItemEnchanted(ItemStack itemStack){
    Map<Enchantment, Integer> enchants = EnchantmentHelper.getEnchantments(itemStack);
    return enchants.size() > 0;
  }

  public int getEnchantmentLevelFromItem(Enchantment enchantment, ItemStack enchantedItem) {
    int selectedEnchantmentLevel = 0;
    Map<Enchantment, Integer> enchantmentMap = EnchantmentHelper.getEnchantments(enchantedItem);
    Iterator<Map.Entry<Enchantment, Integer>> enchantmentIterator = enchantmentMap.entrySet().iterator();

    // Loop through the enchantment count until the random number is hit, then set the enchantment
    for (int loopCounter = 0; loopCounter < enchantmentMap.size(); loopCounter++){
      Map.Entry<Enchantment, Integer> entry = enchantmentIterator.next();
      if(entry.getKey() == enchantment) {
        selectedEnchantmentLevel = entry.getValue();
        break;
      }
    }
    return selectedEnchantmentLevel;
  }

  public Enchantment getEnchantmentFromItem(ItemStack enchantedItem) {
    Enchantment selectedEnchantment = null;
    Map<Enchantment, Integer> enchantmentMap = EnchantmentHelper.getEnchantments(enchantedItem);
    int enchantmentNumber = TileDisenchant.RANDOM_ENCHANTMENT.get() ? ThreadLocalRandom.current().nextInt(0, enchantmentMap.size()) : 0;
    Iterator<Map.Entry<Enchantment, Integer>> enchantmentIterator = enchantmentMap.entrySet().iterator();

    // Loop through the enchantment count until the random number is hit, then set the enchantment
    for (int loopCounter = 0; loopCounter <= enchantmentNumber; loopCounter++){
      Map.Entry<Enchantment, Integer> entry = enchantmentIterator.next();
      if(loopCounter == enchantmentNumber) {
        selectedEnchantment = entry.getKey();
        break;
      }
    }
    return selectedEnchantment;
  }

  @Override
  public boolean stillValid(Player playerIn) {
    return stillValid(ContainerLevelAccess.create(tile.getLevel(), tile.getBlockPos()), playerEntity, BlockRegistry.DISENCHANTER.get());
  }

  @Override
  public ItemStack quickMoveStack(Player playerIn, int index) {
    try {
      /**
       * indices:
       *  0: enchanted item slot
       *  1: book slot
       *  2: enchanted book slot
       *  3-29: inventory
       *  30-30: hotbar
       */

      ItemStack extractedItem = this.slots.get(index).getItem().copy();
      //ModDisenchanting.LOGGER.error("quickMoveStack: fired on index " + index + " with extractedItem " + extractedItem);
      //if last machine slot is 17, endInv is 18
      int playerStart = endInv;
      int playerEnd = endInv + PLAYERSIZE; //53 = 17 + 36
      //standard logic based on start/end
      ItemStack itemstack = ItemStack.EMPTY;
      Slot slot = this.slots.get(index);
      if (slot != null && slot.hasItem()) {
        ItemStack stack = slot.getItem();
        itemstack = stack.copy();
        if (index < this.endInv) {
          if (!this.moveItemStackTo(stack, playerStart, playerEnd, false)) {
            return ItemStack.EMPTY;
          }
        }
        else if (index <= playerEnd && !this.moveItemStackTo(stack, startInv, endInv, false)) {
          return ItemStack.EMPTY;
        }
        if (stack.isEmpty()) {
          slot.set(ItemStack.EMPTY);
          if (index == 2) {
            disenchantItem(playerIn, extractedItem);
          }
        }
        else {
          slot.setChanged();
        }
        if (stack.getCount() == itemstack.getCount()) {
          return ItemStack.EMPTY;
        }

        slot.onTake(playerIn, stack);

      }
      return itemstack;
    }
    catch (Exception e) {
      return ItemStack.EMPTY;
    }
  }

  @Override
  public void initializeContents(int someInt, List<ItemStack> itemStackList, ItemStack itemStack) {
    super.initializeContents(someInt, itemStackList, itemStack);
    createOutputItem();
  }
}
