package com.chirptheboy.disenchanting.block.disenchant;

import com.chirptheboy.disenchanting.ModDisenchanting;
import com.chirptheboy.disenchanting.gui.ScreenBase;
import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.network.chat.Component;
import net.minecraft.world.entity.player.Inventory;

public class ScreenDisenchant extends ScreenBase<ContainerDisenchant> {

  public ScreenDisenchant(ContainerDisenchant screenContainer, Inventory inv, Component titleIn) {
    super(screenContainer, inv, titleIn);
  }

  @Override
  public void init() {
    super.init();
  }

  @Override
  public void render(PoseStack ms, int mouseX, int mouseY, float partialTicks) {
    this.renderBackground(ms);
    super.render(ms, mouseX, mouseY, partialTicks);
    this.renderTooltip(ms, mouseX, mouseY);
  }

  @Override
  protected void renderLabels(PoseStack ms, int mouseX, int mouseY) {
    if (this.menu.tile.showCost()) this.drawCostString(ms, this.title.getString(), this.menu.tile.getCost(), this.menu.tile.getColor());
  }

  @Override
  protected void renderBg(PoseStack ms, float partialTicks, int mouseX, int mouseY) {
    this.drawBackground(ms, ModDisenchanting.DISENCHANTER_GUI);
  }
}
