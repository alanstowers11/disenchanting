package com.chirptheboy.disenchanting.gui;

import com.chirptheboy.disenchanting.block.TileBlockEntityDisenchanting;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.inventory.DataSlot;
import net.minecraft.world.inventory.MenuType;
import net.minecraft.world.inventory.Slot;

public abstract class ContainerBase extends AbstractContainerMenu {

  public static final int PLAYERSIZE = 4 * 9;
  protected Player playerEntity;
  protected Inventory playerInventory;
  protected int startInv = 0;
  protected int endInv = 17; //must be set by extending class
  protected ContainerBase(MenuType<?> type, int id) {
    super(type, id);
  }

  protected void trackAllIntFields(TileBlockEntityDisenchanting tile, int fieldCount) {
    for (int f = 0; f < fieldCount; f++) {
      trackIntField(tile, f);
    }
  }

  protected void trackIntField(TileBlockEntityDisenchanting tile, int fieldOrdinal) {
    addDataSlot(new DataSlot() {

      @Override
      public int get() {

        return tile.getField(fieldOrdinal);
      }

      @Override
      public void set(int value) {
        tile.setField(fieldOrdinal, value);
      }
    });
  }


  private int addSlotRange(Inventory handler, int index, int x, int y, int amount, int dx) {
    for (int i = 0; i < amount; i++) {
      addSlot(new Slot(handler, index, x, y));
      x += dx;
      index++;
    }
    return index;
  }

  private int addSlotBox(Inventory handler, int index, int x, int y, int horAmount, int dx, int verAmount, int dy) {
    for (int j = 0; j < verAmount; j++) {
      index = addSlotRange(handler, index, x, y, horAmount, dx);
      y += dy;
    }
    return index;
  }

  protected void layoutPlayerInventorySlots(int leftCol, int topRow) {
    // Player inventory
    addSlotBox(playerInventory, 9, leftCol, topRow, 9, 18, 3, 18);
    // Hotbar
    topRow += 58;
    addSlotRange(playerInventory, 0, leftCol, topRow, 9, 18);
  }
}
