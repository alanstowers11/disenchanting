package com.chirptheboy.disenchanting.data;

import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.ItemTags;
import net.minecraft.tags.TagKey;
import net.minecraft.world.item.Item;

public class DataTags {

    public static final TagKey<Item> DISENCHANTER_IMMUNE = ItemTags.create(new ResourceLocation("disenchanting:disenchanter_immune"));

    public static void setup() {
        // do not delete:! this makes the mod get classloaded so the wrapper tags correctly get added to the registry early, before recipe testing
    }
}
