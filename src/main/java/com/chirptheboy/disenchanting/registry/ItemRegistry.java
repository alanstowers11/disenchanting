package com.chirptheboy.disenchanting.registry;

import com.chirptheboy.disenchanting.ModDisenchanting;
import net.minecraft.world.item.*;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD)
public class ItemRegistry {

  public static final CreativeModeTab ITEM_GROUP = new CreativeModeTab("Disenchanting") {
    @Override
    public ItemStack makeIcon() {
      return new ItemStack(Items.DIAMOND);
    }
  };

  public static final DeferredRegister<Item> ITEMS = DeferredRegister.create(ForgeRegistries.ITEMS, ModDisenchanting.MODID);
  public static final RegistryObject<Item> DISENCHANTER = ITEMS.register("disenchanter", () -> new BlockItem(BlockRegistry.DISENCHANTER.get(), new Item.Properties().tab(ITEM_GROUP)));
}
