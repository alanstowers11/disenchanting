package com.chirptheboy.disenchanting.registry;

import com.chirptheboy.disenchanting.block.BlockDisenchanting;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;

public class ClientRegistryDisenchanting {

    public ClientRegistryDisenchanting() {}

    public static void setupClient(final FMLClientSetupEvent event) {
        for (BlockDisenchanting b : BlockRegistry.BLOCKSCLIENTREGISTRY) {
            b.registerClient();
        }
    }
}
